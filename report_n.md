# Entrega dos exercícios

- **Grupo**: ds122-2021-2-n
- **Última atualização**: sex 29 abr 2022 15:01:04 -03

|Nome|	ds122-prepare-assignment<br>2022-04-30|	ds122-html-tables-assignment<br>2022-04-30|	ds122-html-store-assignment<br>2022-04-30|	ds122-css-assignment<br>2022-04-30|	ds122-css-position-assignment<br>2022-04-30|	ds122-youtube-assignment<br>2022-04-30|	ds122-js-exercises-assignment<br>2022-04-30|	ds122-dom-assignment<br>2022-04-30|	ds122-jquery-assignment<br>2022-04-30|	ds122-php-assignment<br>2022-04-30|	ds122-php-form-assignment<br>2022-04-30|
|----|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|
|ADRIANO_DE_OLIVEIRA_BRAGANHOL|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ALINE_MACHADO_LIMA|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ANDRE_LUIZ_OLMEDO|	 Fork não encontrado |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ARTHUR_NASCIMENTO|	 Fork não encontrado |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|CAROLINA_DOS_SANTOS_DE_LIMA|	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|CHRISTOPHER_PICOLOTTO_RODRIGUES|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|DIEGO_VIEIRA_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|DOUGLAS_SCHEFFER_LUBIAN|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|EMANUELLY_WOLSKI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|EMILLY_WOLSKI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|ERICK_DJOE_PEREIRA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|FABIO_FERREIRA_DA_SILVA|	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|FELIPE_GABRIEL_REIMER|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |
|GABRIEL_RICHTER_SANDRI|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|GUSTAVO_CANDIDO_DE__ALMEIDA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|HAMAD_RASLAN_QUEIROZ|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|
|IMAN_DE_LACERDA_CHARAFEDDINE|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ISABELLE_DOS_SANTOS_CASSIANO|	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 ok |
|JOÃO_ALBERTO_FRANÇOIS|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|JUAN_CARLO_FF._R._R._DE_MORAIS|	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|JULIO_MITSUJI_YAMAGUCHI_LIMEIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 ok |	 ok |
|LUCAS_FRISON_GONÇALVES|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|LUCAS_LEONEL_NEVES|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|LUIZ_FERNANDO_DA_SILVEIRA_GHISI|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 ok |	 ok |	 ok |	 ok |
|LUIZ_SENJI_VIDAL_TSUCHIYA|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |	 ok |	 ok |
|MARICOT_NICOLAS|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MATEUS_IRINEU_GUIMARAES_MALLMANN|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|MATHEUS_ANTONIO_KERSCHER|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|MATHEUS_RIGLER|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|MONICA_RODRIGUES_DOS_SANTOS|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |
|NATHAN_DOS_SANTOS_ROSA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|RAFAEL_DIAS_SIMEONI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|RAPHAEL_PERES_DE_OLIVEIRA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|RICKSON_GIOVANNI_ROCHA_DE_PAULA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|SUSAN_CRISTINI_DE_SOUZA_ALMEIDA|	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|TIAGO_FRANCISCHINI|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|TIAGO_SHIGUEO_YAJIMA_YAMASAKI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |
|VINICIUS_EDUARDO_CHACA_FERREIRA|	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
