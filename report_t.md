# Entrega dos exercícios

- **Grupo**: ds122-2021-2-t
- **Última atualização**: sex 29 abr 2022 15:28:00 -03

|Nome|	ds122-prepare-assignment<br>2022-04-30|	ds122-html-tables-assignment<br>2022-04-30|	ds122-html-store-assignment<br>2022-04-30|	ds122-css-assignment<br>2022-04-30|	ds122-css-position-assignment<br>2022-04-30|	ds122-youtube-assignment<br>2022-04-30|	ds122-js-exercises-assignment<br>2022-04-30|	ds122-dom-assignment<br>2022-04-30|	ds122-jquery-assignment<br>2022-04-30|	ds122-php-assignment<br>2022-04-30|	ds122-php-form-assignment<br>2022-04-30|
|----|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|
|ALEIDA_MOPI_LAFUENTE|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ANDRE_RICARDO_AUBIM_DE_ARAUJO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|
|ANITA_SANTOS_TEIXEIRA|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 ok |	 ok |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|ARTHUR_BORGES_TOSO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|ARTHUR_GIAN_ALVES_DE_OLIVEIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|
|ARUNI_SERENA_VAN_AMSTEL|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|BRITTANY_GREY_PINA_OSORIO|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |
|CAROL_GOMES_GALESKI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|
|DAVI_FERREIRA_COLARES|	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|EDUARDO_FELIX_DA_SILVA_NETO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|FATIMA_DOS_SANTOS_KRAICZYI|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |
|GEOVANNA_ALBERTI_CORREIA_DE_FREITAS|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GIANLUCA_NOTARI_MAGNABOSCO_DA_SILVA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GUILHERME_CICARELLO_GIRATA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GUILHERME_FRANCO_BATISTA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GUILHERME_PENNA_MORO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|IGOR_NATHAN_LOBATO|	 ok |	 Fork não encontrado |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|JEFFERSON_SANTANA_FILHO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |
|JOÃO_VITOR_ANDRADE_DA_SILVA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|LEONARDO_CHUASTE|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|LEONARDO_VZOREK|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|
|LUCAS_HIDEKI_LOPES_COELHO_IKEDA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|LUCAS_MACHADO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|LUIZ_FELIPE_TOZATI|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|LUIZ_HENRIQUE_SCHECHELI_BUSSOLO|	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|MARCELO_BATISTA_DA_LUZ_JUNIOR|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MARCO_ANTONIO_LAMPERT_JUNKES|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |
|MARCOS_FELIPE_LOPES_RODRIGUES|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MARINA_NEVES_BEPPLER|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|MATHEUS_CORREA_FIORI|	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MURILO_SCHRICKTE|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|NICHOLAS_MANOEL_ANTUNES_DE_PONTES|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 Fork não encontrado |	 ok |	 ok |
|PEDRO_ROMERO_MASSEDO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|PIETRA_PIZZATTO_MINATTI|	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 ok |	 ok |	 Fork não encontrado |
|PIETRO_BORGES_PARRI|	 ok |	 ok |	 Fork não encontrado |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|RENAN_KENJI_KOGA_KURODA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|THÉO_DREER_TAVARES|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|THIAGO_DOS_SANTOS|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|VINICIUS_RATZKE_SERVELO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|YASMIN_ALLANNY_CALDERON_SILVA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|
